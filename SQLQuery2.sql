select * from Users
select * from Status
select * from Guests
select * from GuestClasses
select * from Classes
SELECT * FROM RoomStay
SELECT * FROM Rooms
SELECT * FROM Taverns

insert into Status
values
('Healthy', 'All good'),
('Sick', 'Not good'),
('Poisoned', 'Poisoned'),
('On Fire', 'Hot!')

INSERT INTO Guests
values 
(1, 5, '9-9-1990'),
(2, 6, '9-9-1960'),
(3, 5, '1-12-1970'),
(4, 8, '9-9-1977'),
(5, 5, '4-20-1988')

INSERT INTO Classes
values
(1, 'Mage', 'Wizard!'),
(2,'Archer', 'Bows n stuff'),
(3, 'Warrior', 'Swords'),
(4, 'Fighter', 'Fisticuffs'),
(5, 'Thief', 'Slick')

INSERT INTO GuestClasses
values
(2, 5, 50),
(2, 4, 12),
(3, 1, 30),
(4, 2, 4),
(5, 3, 29),
(5, 4, 35),
(6, 1, 5),
(6, 2, 10)

INSERT INTO Rooms
values
(1, 1, 1, 100),
(2, 1, 1, 120),
(3, 1, 0, 145),
(1, 2, 1, 100),
(2, 3, 1, 120),
(3, 3, 0, 145),
(1, 6, 1, 100),
(2, 6, 1, 120),
(3, 6, 0, 145),
(1, 7, 1, 300),
(2, 7, 1, 220),
(3, 7, 0, 245)

INSERT INTO RoomStay
values
(2, 1, 1, NULL, '09-10-2018', 100),
(3, 2, 1, NULL, '10-10-2018', 110),
(4, 1, 6, NULL, '10-11-2018', 105),
(5, 3, 7, NULL, '10-17-2018', 100),
(6, 2, 7, NULL, '09-10-2018', 300)