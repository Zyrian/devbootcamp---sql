DROP TABLE IF EXISTS [Taverns];
CREATE TABLE Taverns (
	ID int IDENTITY (1,1),
	TavernName varchar(100),
	LocationID int,
	OwnerID int
);

DROP TABLE IF EXISTS [Locations];
CREATE TABLE Locations (
	LocationID int IDENTITY (1,1),
	LocationName varchar(100),
);

DROP TABLE IF EXISTS [Users];
CREATE TABLE Users (
	UserID int IDENTITY (1,1),
	UserName varchar(250),
	RoleID int
);

DROP TABLE IF EXISTS [Roles];
CREATE TABLE Roles (
	RoleID int IDENTITY (1,1),
	RoleName varchar(50),
	Description varchar(MAX)
);

DROP TABLE IF EXISTS [BasementRats];
CREATE TABLE BasementRats (
	RatID int,
	RatName varchar(100)
);