DROP TABLE IF EXISTS Rooms;
CREATE TABLE Rooms (
	ID int NOT NULL,
	TavernID int FOREIGN KEY REFERENCES Taverns(ID),
	Available int DEFAULT 1,
	Rate smallmoney
	PRIMARY KEY (ID, TavernID)
)

DROP TABLE IF EXISTS Status;
CREATE TABLE Status (
	ID int IDENTITY (1,1),
	Name varchar(100),
	Description varchar(100),
	PRIMARY KEY (ID)
)

DROP TABLE IF EXISTS Guests;
CREATE TABLE Guests (
	ID int IDENTITY (1,1) PRIMARY KEY,
	UserID int FOREIGN KEY REFERENCES Users(ID),
	Status int FOREIGN KEY REFERENCES Status(ID),
	Birthdate date
)

DROP TABLE IF EXISTS Classes;
CREATE TABLE Classes (
	ID int IDENTITY (1,1) PRIMARY KEY,
	Name varchar(100),
	Description varchar(MAX)
)

DROP TABLE IF EXISTS GuestClasses;
CREATE TABLE GuestClasses (
	GuestID int FOREIGN KEY REFERENCES Guests(ID),
	ClassID int FOREIGN KEY REFERENCES Classes(ID),
	Level int,
	PRIMARY KEY (GuestID, ClassID)
)

DROP TABLE IF EXISTS RoomStay;
CREATE TABLE RoomStay (
	GuestID int FOREIGN KEY REFERENCES Guests(ID),
	RoomID int,
	TavernID int,
	SaleID int FOREIGN KEY REFERENCES SupplySales(ID),
	DateStayed date,
	Rate smallmoney,

	--Referencing compound key in Rooms table, so I had to add both ID and TavernID as foreign keys
	CONSTRAINT PK_Rooms FOREIGN KEY (RoomID, TavernID) REFERENCES Rooms(ID, TavernID)
)

--Return guests with a birthday before 2000
SELECT u.FirstName, u.LastName, g.Birthdate 
FROM Guests as g
JOIN Users as u ON u.ID = g.UserID
WHERE g.Birthdate < '01-01-2000';

--Return rooms that cost more than 100 gold per night
SELECT t.Name, r.ID, r.Rate
FROM Rooms as r
JOIN Taverns as t ON t.ID = r.TavernID
WHERE r.Rate > 100;

--Return unique guest names
SELECT DISTINCT u.FirstName, u.LastName, g.Birthdate
FROM Guests as g
JOIN Users as u ON u.ID = g.UserID;

--Return all guests ordered by name
SELECT u.FirstName, u.LastName, g.Birthdate
FROM Guests as g
JOIN Users as u ON u.ID = g.UserID
ORDER BY u.FirstName ASC;

--Returns top 10 highest priced sales
SELECT TOP 10 *
FROM SupplySales as ss
JOIN Supplies as s ON s.ID = ss.SupplyID
JOIN Taverns as t ON t.ID = ss.TavernID
ORDER BY ss.Price DESC;

--Returns all Lookup Table Names
SELECT Name, ID FROM Roles
UNION ALL
SELECT Name, ID FROM Services
UNION ALL 
SELECT Name, ID FROM Status
UNION ALL
SELECT Name, ID FROM Classes
UNION ALL
SELECT Name, ID FROM Locations

--Return guest classes and grouping
SELECT u.FirstName, u.LastName, cl.Name, gc.Level, 
(CASE 
WHEN Level <= 10 THEN '1 - 10'
WHEN Level <= 20 THEN '11 - 20'
WHEN Level <= 30 THEN '21 - 30'
WHEN Level <= 40 THEN '31 - 40'
WHEN Level <= 50 Then '41 - 50'
END)as Grouping
FROM Guests as g
JOIN GuestClasses as gc ON gc.GuestID = g.ID
JOIN Classes as cl ON cl.ID = gc.ClassID
JOIN Users as u ON u.ID = g.UserID

--Insert statuses of one table into another
SELECT u.FirstName, u.LastName, s.Name INTO GuestStatus
FROM Guests as g
JOIN Status as s ON s.ID = g.Status
JOIN Users as u ON u.ID = g.UserID