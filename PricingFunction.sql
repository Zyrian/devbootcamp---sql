IF OBJECT_ID(N'dbo.pricing', N'IF') IS NOT NULL
	DROP FUNCTION dbo.pricing;
GO
CREATE FUNCTION dbo.pricing (@price int, @quantity int)
RETURNS int
AS
BEGIN
	DECLARE @ret int;
	SELECT @ret = (@price * @quantity)
	IF (@ret IS NULL)
		SET @ret = 0;
	RETURN @ret;
END

SELECT dbo.pricing(5, 6);