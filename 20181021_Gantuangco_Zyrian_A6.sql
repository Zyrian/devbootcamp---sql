-- Return a report of all users and their roles
IF OBJECT_ID (N'dbo.userReport', N'IF') IS NOT NULL
	DROP FUNCTION dbo.userReport;
GO
CREATE FUNCTION dbo.userReport()
RETURNS TABLE
AS
RETURN (
	SELECT u.FirstName, u.LastName, r.Name
	FROM Users as u
	JOIN Roles as r ON r.ID = u.RoleID
);

SELECT * FROM dbo.userReport()

-- Return all classes and the count of guests that hold those classes
SELECT c.Name, COUNT(*) as Count
FROM GuestClasses as gc
JOIN Classes as c ON c.ID = gc.ClassID
JOIN Guests as g ON g.ID = gc.GuestID
GROUP BY c.Name

-- Return all guests ordered by name and their classes/corresponding levels.
-- Add a column that labels them beginner (lv 1-5), intermediate (5-10) and expert (10+) for their classes
SELECT u.FirstName, u.LastName, c.Name, gc.Level, 
(CASE
	WHEN Level <= 5 THEN 'Beginner'
	WHEN Level <= 10 THEN 'Intermediate'
	WHEN Level > 10 THEN 'Expert'
END) as Grouping
FROM Guests as g
JOIN Users as u ON u.ID = g.UserID
JOIN GuestClasses as gc ON gc.GuestID = g.ID
JOIN Classes as c ON c.ID = gc.ClassID
ORDER BY u.FirstName ASC

-- Write a function that takes a level and returns a grouping from question 3
IF OBJECT_ID (N'dbo.returnGrouping', N'IF') IS NOT NULL
	DROP FUNCTION dbo.returnGrouping;
GO
CREATE FUNCTION dbo.returnGrouping (@level int)
RETURNS varchar(100)
AS
BEGIN
	DECLARE @group varchar(100);
	SELECT @group = (
		CASE
			WHEN @level <= 5 THEN 'Beginner'
			WHEN @level <= 10 THEN 'Intermediate'
			WHEN @level > 10 THEN 'Expert'
		END)
	IF (@group IS NULL)
		SET @group = 0;
	RETURN @group;
END

SELECT dbo.returnGrouping(10)

-- Write a function that returns a report of all open rooms (not used) on a particular day (input) and which tavern they belong to
IF OBJECT_ID (N'dbo.openRooms', N'IF') IS NOT NULL
	DROP FUNCTION dbo.openRooms;
GO
CREATE FUNCTION dbo.openRooms (@date date)
RETURNS TABLE
AS
RETURN (
	SELECT r.ID, t.Name, r.Rate
	FROM Rooms as r
	JOIN RoomStay as rs ON rs.RoomID = r.ID AND rs.TavernID = r.TavernID
	JOIN Taverns as t ON t.ID = r.TavernID
	WHERE rs.DateStayed != @date AND r.Available = 1
);

SELECT * FROM dbo.openRooms('10-10-2018')

-- Modify the same function from 5 to instead return a report of prices in a range (min and max) - Return rooms and their taverns based on price inputs
IF OBJECT_ID (N'dbo.priceRange', N'IF') IS NOT NULL
	DROP FUNCTION dbo.priceRange;
GO
CREATE FUNCTION dbo.priceRange (@min smallmoney, @max smallmoney)
RETURNS TABLE
AS
RETURN (
	SELECT r.ID as Room, t.Name, r.Rate
	FROM Rooms as r
	JOIN Taverns as t ON t.ID = r.TavernID
	WHERE r.Rate > @min AND r.Rate <= @max AND r.Available = 1
);

SELECT * FROM dbo.priceRange (1, 150)

-- Write a command that uses the result from 6 to create a room in another tavern that undercuts the cheapest room by a penny - thereby making the new room the cheapest one
INSERT INTO Rooms
SELECT TOP 1 (
	SELECT TOP 1 ID + 1
	FROM Rooms
	WHERE TavernID = 7
	ORDER BY ID DESC
	),
7, 1, 
CAST((Rate - 0.01) AS smallmoney)
FROM dbo.priceRange (1, 250) as pr
WHERE pr.Name != 'Krusty Krab'

SELECT * FROM dbo.priceRange (1, 150) ORDER BY Rate