-- Add Supply Sales table with foreign keys
DROP TABLE IF EXISTS SupplySales;
CREATE TABLE SupplySales (
	ID int IDENTITY (1,1) PRIMARY KEY,
	TavernID int FOREIGN KEY REFERENCES Taverns(ID),
	GuestID int FOREIGN KEY REFERENCES Users(ID),
	SupplyID int FOREIGN KEY REFERENCES Supplies(ID),
	Price money,
	DatePurchased date,
	Quantity int
);

-- Add foreign and primary keys to existing tables
ALTER TABLE BasementRats
ADD TavernID int FOREIGN KEY REFERENCES Taverns(ID);

ALTER TABLE TavernServices
ADD PRIMARY KEY (TavernID, ServiceID);

ALTER TABLE SupplyShipment
ADD PRIMARY KEY (SupplyID, TavernID);

ALTER TABLE SupplyInventory
ADD PRIMARY KEY (SupplyID, TavernID);

-- Queries will fail due to foreign key constraints: TavernID and GuestID do not exist in the db
INSERT INTO SupplyShipment
values
(99, 226, 100, 100, '2018-10-6'),
(98, 226, 200, 100, '2018-10-6'),
(98, 225, 100, 50, '2018-10-5'),
(97, 225, 245, 100, '2018-10-5'),
(97, 224, 400, 200, '2018-10-3'),
(96, 226, 200, 200, '2018-10-3');

INSERT INTO SupplyInventory
values
(99, 226, 100, '2018-10-6'),
(98, 226, 100, '2018-10-6'),
(98, 225, 50, '2018-10-5'),
(97, 225, 100, '2018-10-5'),
(97, 224, 200, '2018-10-3'),
(96, 226, 200, '2018-10-3');

INSERT INTO SupplySales
values
(226, 1, 99, 12.50, '2018-10-6', 6),
(226, 7, 98, 20.00, '2018-10-6', 2),
(226, 5, 97, 31.75, '2018-10-6', 10);