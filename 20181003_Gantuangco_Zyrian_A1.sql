DROP TABLE IF EXISTS [Locations];
CREATE TABLE Locations (
	ID int IDENTITY (1,1) PRIMARY KEY,
	Name varchar(100)
);

DROP TABLE IF EXISTS [Services];
CREATE TABLE Services (
	ID int IDENTITY (1,1) PRIMARY KEY,
	Name varchar(100)
);

DROP TABLE IF EXISTS [ServiceStatus];
CREATE TABLE ServiceStatus (
	ID int IDENTITY (1,1) PRIMARY KEY,
	Status varchar(100)
);

DROP TABLE IF EXISTS [Supplies];
CREATE TABLE Supplies (
	ID int IDENTITY (1,1) PRIMARY KEY,
	Name varchar(100),
	Unit varchar(100)
);

DROP TABLE IF EXISTS [Roles];
CREATE TABLE Roles (
	ID int IDENTITY (1,1) PRIMARY KEY,
	Name varchar(100),
	Description varchar(MAX)
);

DROP TABLE IF EXISTS [BasementRats];
CREATE TABLE BasementRats (
	ID int IDENTITY (1,1) PRIMARY KEY,
	Name varchar(100)
);

DROP TABLE IF EXISTS [Users];
CREATE TABLE Users (
	ID int IDENTITY (1,1) PRIMARY KEY,
	FirstName varchar(100),
	LastName varchar(100),
	RoleID int FOREIGN KEY REFERENCES Roles(ID)
);

DROP TABLE IF EXISTS [Taverns];
CREATE TABLE Taverns (
	ID int IDENTITY (1,1) PRIMARY KEY,
	Name varchar(100),
	LocationID int FOREIGN KEY REFERENCES Locations(ID),
	OwnerID int FOREIGN KEY REFERENCES Users(ID)
);

DROP TABLE IF EXISTS [TavernServices];
CREATE TABLE TavernServices (
	TavernID int FOREIGN KEY REFERENCES Taverns(ID),
	ServiceID int FOREIGN KEY REFERENCES Services(ID),
	ServiceStatusID int FOREIGN KEY REFERENCES ServiceStatus(ID)
);

DROP TABLE IF EXISTS [SupplyInventory];
CREATE TABLE SupplyInventory (
	SupplyID int FOREIGN KEY REFERENCES Supplies(ID),
	TavernID int FOREIGN KEY REFERENCES Taverns(ID),
	Count int,
	Updated date
);

DROP TABLE IF EXISTS [SupplyShipment];
CREATE TABLE SupplyShipment (
	SupplyID int FOREIGN KEY REFERENCES Supplies(ID),
	TavernID int FOREIGN KEY REFERENCES Taverns(ID),
	Cost money,
	Quantity int,
	ReceiveDate date
);

DROP TABLE IF EXISTS [ServiceSales];
CREATE TABLE ServiceSales (
	ID int IDENTITY (0000001,1) PRIMARY KEY,
	TavernID int FOREIGN KEY REFERENCES Taverns(ID),
	GuestID int FOREIGN KEY REFERENCES Users(ID),
	ServiceID int FOREIGN KEY REFERENCES Services(ID),
	Price money,
	DatePurchased date,
	Quantity int,
);

INSERT INTO Locations
(Name)
values
('Knowhere'),
('Asgard'),
('Latveria'),
('Genosha'),
('Gotham'),
('Metropolis'),
('Bludhaven'),
('Wakanda');

INSERT INTO Services
(Name)
values
('Pool'),
('Bowling'),
('Go Kart'),
('Weapon Sharpening'),
('Shooting Range'),
('Sparring');

INSERT INTO ServiceStatus
(Status)
values
('active'),
('inactive'),
('out of stock'),
('discontinued'),
('preorder');

INSERT INTO Supplies
(Name, Unit)
values
('Strong Ale', 'ounce'),
('Weak Ale', 'ounce'),
('Duff', 'can'),
('Slurm', 'can'),
('Soddie Pop', 'bottle'),
('POWERTHIRST', 'CAN');

INSERT INTO Roles
(Name, Description)
values
('Owner', 'Big boss man'),
('Guest', 'Customer, visitor, client, patron, etc.'),
('Bartender', 'Serves drinks'),
('Busboy/girl', 'Cleans up'),
('Waiter/Waitress', 'Takes orders and brings food to tables'),
('Cook', 'Makes food');

INSERT INTO BasementRats
(Name)
values
('Rick'),
('James'),
('Morty'),
('Remy'),
('Splinter'),
('Wormtail');

INSERT INTO Users
(FirstName, LastName, RoleID)
values
('Peter', 'Parker', 4),
('Uncle', 'Ben', 1),
('Moe', 'Szyslak', 3),
('Rob', 'Schneider', 2),
('Dennis', 'Reynolds', 3),
('Chef', '', 5),
('Rick', 'Fox', 1),
('Bill', 'Gates', 2),
('King', 'T''Challa', 1);

INSERT INTO Taverns
(Name, LocationID, OwnerID)
values
('Test Tavern', 1, 2),
('Tasty Beer', 2, 7),
('Best Booze', 3, 3),
('Fiddle Me Timbers', 4, 2),
('Fiddle Me Timbers', 4, 2),
('Chapanya', 8, 9),
('Krusty Krab', 6, 3);

INSERT INTO TavernServices
(TavernID, ServiceID, ServiceStatusID)
values
(1, 2, 1),
(1, 2, 5),
(3, 1, 2),
(4, 5, 4),
(4, 1, 1),
(5, 2, 1),
(2, 2, 2),
(2, 2, 1),
(3, 3, 4),
(5, 5, 5);

INSERT INTO SupplyInventory
(SupplyID, TavernID, Count, Updated)
values
(6, 4, 2, '2018-10-02'),
(6, 7, 200, '2018-01-01'),
(5, 3, 31, '2012-12-25'),
(5, 1, 22, '2018-08-25'),
(3, 2, 63, '2018-05-02'),
(3, 6, 22, '2018-07-23'),
(2, 5, 97, '2018-04-20'),
(5, 5, 234, '2018-03-06');

INSERT INTO SupplyShipment
(SupplyID, TavernID, Cost, Quantity, ReceiveDate)
values
(6, 4, 10.02, 2, '2018-10-02'),
(6, 7, 222.45, 200, '2018-01-01'),
(5, 3, 31.50, 31, '2012-12-25'),
(5, 1, 50.20, 22, '2018-08-25'),
(3, 2, 9.99, 63, '2018-05-02'),
(3, 6, 9.99, 22, '2018-07-23'),
(2, 5, 10.70, 97, '2018-04-20'),
(5, 5, 250, 234, '2018-03-06');

INSERT INTO ServiceSales
(TavernID, GuestID, ServiceID, Price, DatePurchased, Quantity)
values
(1, 3, 5, 9.99, '2018-10-03', 1),
(2, 5, 2, 19.99, '2018-10-03', 2),
(5, 7, 4, 2.50, '2018-10-03', 1),
(6, 1, 6, 12.00, '2018-10-03', 6),
(4, 2, 1, 1.99, '2018-10-03', 1),
(7, 6, 3, 119.99, '2018-10-03', 56),
(6, 3, 5, 29.99, '2018-10-03', 5),
(2, 3, 5, 9.99, '2018-10-03', 8);