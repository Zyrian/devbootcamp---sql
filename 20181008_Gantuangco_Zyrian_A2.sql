SELECT * FROM Taverns
SELECT * FROM Users
SELECT * FROM Services
SELECT * FROM TavernServices
SELECT * FROM Supplies
SELECT * FROM SupplyInventory
SELECT * FROM SupplyShipment

DROP TABLE IF EXISTS SupplySales;
CREATE TABLE SupplySales (
	ID int IDENTITY (1,1) PRIMARY KEY,
	TavernID int FOREIGN KEY REFERENCES Taverns(ID),
	GuestID int FOREIGN KEY REFERENCES Users(ID),
	SupplyID int FOREIGN KEY REFERENCES Supplies(ID),
	Price money,
	DatePurchased date,
	Quantity int
);

INSERT INTO Users
(FirstName, LastName, RoleID)
values
('Gordon','Ramsay', 1),
('Sweeny','Todd', 1);

INSERT INTO Taverns
(Name, LocationID, OwnerID)
values
('Pub Planet', 7, 7),
('Drunken Beast', 8, 9),
('Hell''s Kitchen', 3, 20),
('Meaty Pies', 7, 21),
('Pub & Grill', 6, 20);

INSERT INTO Services
values
('Enchantments'),
('Armor Dyes'),
('Featured Film');

UPDATE Services
SET Name = 'Boxing'
WHERE Name = 'Sparring';

UPDATE TavernServices
SET TavernID = 4
WHERE TavernID = 5;

UPDATE SupplyInventory
SET TavernID = 4
WHERE TavernID = 5;

UPDATE SupplyShipment
SET TavernID = 4
WHERE TavernID = 5;

UPDATE ServiceSales
SET TavernID = 4
WHERE TavernID = 5;

DELETE FROM Taverns
WHERE ID = 5;

--Shipment and inventory updates 01
INSERT INTO SupplyShipment
values
(6, 26, 100, 100, '2018-10-6'),
(5, 26, 200, 100, '2018-10-6'),
(5, 25, 100, 50, '2018-10-5'),
(4, 25, 245, 100, '2018-10-5'),
(4, 24, 400, 200, '2018-10-3'),
(3, 26, 200, 200, '2018-10-3');

INSERT INTO SupplyInventory
values
(6, 26, 100, '2018-10-6'),
(5, 26, 100, '2018-10-6'),
(5, 25, 50, '2018-10-5'),
(4, 25, 100, '2018-10-5'),
(4, 24, 200, '2018-10-3'),
(3, 26, 200, '2018-10-3');

--Tracking sales 01
INSERT INTO SupplySales
values
(26, 1, 6, 12.50, '2018-10-6', 6),
(26, 7, 5, 20.00, '2018-10-6', 2),
(26, 5, 3, 31.75, '2018-10-6', 10);

UPDATE SupplyInventory
SET Count = Count - 6, Updated = '2018-10-6'
WHERE SupplyID = 6 AND TavernID = 26;

UPDATE SupplyInventory
SET Count = Count - 2, Updated = '2018-10-6'
WHERE SupplyID = 5 AND TavernID = 26;

UPDATE SupplyInventory
SET Count = Count - 10, Updated = '2018-10-6'
WHERE SupplyID = 3 AND TavernID = 26;


--Shipment and inventory updates 02
INSERT INTO SupplyShipment
values
(6, 26, 100, 100, '2018-10-7'),
(5, 26, 200, 100, '2018-10-7'),
(3, 26, 200, 200, '2018-10-7');

UPDATE SupplyInventory
SET Count = Count + 100, Updated = '2018-10-7'
WHERE SupplyID = 6 AND TavernID = 26;

UPDATE SupplyInventory
SET Count = Count + 100, Updated = '2018-10-7'
WHERE SupplyID = 5 AND TavernID = 26;

UPDATE SupplyInventory
SET Count = Count + 200, Updated = '2018-10-7'
WHERE SupplyID = 3 AND TavernID = 26;


--Tracking sales 02
INSERT INTO SupplySales
values
(26, 3, 6, 10.50, '2018-10-7', 2),
(26, 5, 5, 20.00, '2018-10-7', 5),
(26, 8, 3, 3.25, '2018-10-7', 1);

UPDATE SupplyInventory
SET Count = Count - 2, Updated = '2018-10-7'
WHERE SupplyID = 6 AND TavernID = 26;

UPDATE SupplyInventory
SET Count = Count - 5, Updated = '2018-10-7'
WHERE SupplyID = 5 AND TavernID = 26;

UPDATE SupplyInventory
SET Count = Count - 1, Updated = '2018-10-7'
WHERE SupplyID = 3 AND TavernID = 26;
GO

CREATE VIEW MonthlySales AS
SELECT tav.Name as TavernName, sup.Name as SupplyName, sales.Quantity, sales.Price, sales.DatePurchased
FROM SupplySales as sales
JOIN Supplies as sup ON sales.SupplyID = sup.ID
JOIN Taverns as tav ON sales.TavernID = tav.ID;
GO

SELECT * FROM MonthlySales