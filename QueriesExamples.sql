use SQLDBBootCamp

SELECT* FROM USERS 

SELECT NULLIF(Rol--eId, 1) as nullish FROM Users 
SELECT COALESCE(Notes, SuperIdentity, 'Oh no') AS UserInfo FROM USERS

/*
, NULLIF(RoleId, 1) as nullish
*/

SELECT CAST(roleId as decimal(18,5)) from Users;

SELECT CONCAT('Hello, ', USERS.Name, '. Welkommen') as Welcome FROM Users

SELECT RoleId, 
(CASE When RoleId = 1 then
 'One' else 'Not-one' end) 
 as RoleIdIsOne FROM USERS;


SELECT * FROM Taverns
WHERE EXISTS 
(SELECT 1 FROM ServicesSales 
Where ServicesSales.TavernId = Taverns.ID)

select * from users
WHERE Users.Id in (SELECT Id FROM Roles)


SELECT DISTINCT RoleId From Users
SELECT DISTINCT RoleId, Name From Users

SELECT * FROM Users WHERE Users.Id BETWEEN 1 AND 50

SELECT (CASE WHEN RoleID = 1 THEN 
'Role 1' WHEN RoleID = 2 THEN
 'Role 2' ELSE 'Role Else' END) as RoleTest FROM Users





SELECT Name, Id, 'roles' as tbl from roles /*'roles' as tbl*/
UNION ALL
SELECT Name, Id, 'status' as tbl from GuestStatus /*'status' as tbl*/



SELECT Id from roles /*'roles' as tbl*/
INTERSECT
SELECT Id from GuestStatus /*'status' as tbl*/
ORDER By Id


SELECT * From Taverns
EXCEPT
SELECT * From Taverns
WHERE Id > 5


SELECT * FROM ServicesSales order by price, quantitypurchased desc
/* [Group By] [Order By] */

select * INTO TavernBackup02 FROM Taverns

select * from Users for xml auto

/*select 'Hello'
*/
/*
SELECT *
FROM INFORMATION_SCHEMA.COLUMNS
*/
select 'hello'
FROM INFORMATION_SCHEMA.COLUMNS
SELECT *
FROM INFORMATION_SCHEMA.TABLES WHERE table_Name = 'Taverns'
select * from sys.all_columns
/*
CREATE TABLE Test (
	ID int,
	Name varchar(100)
)
*/

SELECT 
CONCAT('CREATE TABLE ',TABLE_NAME, ' (') as queryPiece 
FROM INFORMATION_SCHEMA.TABLES
 WHERE TABLE_NAME = 'Taverns'
UNION ALL
SELECT CONCAT(cols.COLUMN_NAME, ' ', cols.DATA_TYPE, 
(CASE WHEN CHARACTER_MAXIMUM_LENGTH IS NOT NULL 
Then CONCAT('(', CAST(CHARACTER_MAXIMUM_LENGTH as varchar(100)), 
')') Else '' END), ',') as queryPiece FROM 
INFORMATION_SCHEMA.COLUMNS as cols WHERE
TABLE_NAME = 'Taverns'
UNION ALL
SELECT ')';
