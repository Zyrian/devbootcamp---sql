-- Return users with admin roles
SELECT u.FirstName, u.LastName
FROM Users as u
JOIN Roles as r ON r.ID = u.RoleID
WHERE r.Name = 'Admin'

-- Return users with admin roles and their taverns
SELECT u.FirstName, u.LastName, t.Name as TavernName, l.Name as Location
FROM Users as u
JOIN Roles as r ON r.ID = u.RoleID
JOIN Taverns as t ON t.OwnerID = u.ID
JOIN Locations as l ON l.ID = t.LocationID
WHERE r.Name = 'Admin'

-- Return all guests ordered by name (ascending) and their classes/levels
SELECT u.FirstName, u.LastName, c.Name, gc.Level
FROM Users as u
JOIN Guests as g ON g.UserID = u.ID
JOIN GuestClasses as gc ON gc.GuestID = g.ID
JOIN Classes as c ON c.ID = gc.ClassID
ORDER BY u.FirstName ASC

-- Return top 10 sales and what the services were
SELECT TOP 10 t.Name, u.FirstName, u.LastName, s.Name, ss.Price, ss.Quantity, ss.DatePurchased
FROM ServiceSales as ss
JOIN Services as s ON s.ID = ss.ServiceID
JOIN Guests as g ON g.ID = ss.GuestID
JOIN Users as u ON u.ID = g.UserID
JOIN Taverns as t ON t.ID = ss.TavernID

-- Return guests with 2 or more classes
SELECT u.FirstName, u.LastName, COUNT(*) as Count
FROM Users as u
JOIN Guests as g ON g.UserID = u.ID
JOIN GuestClasses as gc ON gc.GuestID = g.ID
JOIN Classes as c ON c.ID = gc.ClassID
GROUP BY u.FirstName, u.LastName
HAVING COUNT(*) >= 2


-- Return guests with 2 or more classes with levels higher than 5
SELECT u.FirstName, u.LastName, COUNT(*) as Count
FROM Users as u
JOIN Guests as g ON g.UserID = u.ID
JOIN GuestClasses as gc ON gc.GuestID = g.ID
JOIN Classes as c ON c.ID = gc.ClassID
WHERE gc.Level >= 5
GROUP BY u.FirstName, u.LastName
HAVING COUNT(*) >= 2
ORDER BY u.FirstName ASC

-- Return guests with only their highest level class
-- Adding class names to this query returns all classes, so it was removed to show ONLY the highest levels
SELECT u.FirstName, u.LastName, MAX(gc.Level)as HighestLevel
FROM Users as u
JOIN Guests as g ON g.UserID = u.ID
JOIN GuestClasses as gc ON gc.GuestID = g.ID
JOIN Classes as c ON c.ID = gc.ClassID
GROUP BY u.FirstName, u.LastName
ORDER BY HighestLevel ASC

-- Return guests that stay within a date range
SELECT u.FirstName, u.LastName, rs.DateStayed
FROM RoomStay as rs
JOIN Guests as g ON g.ID = rs.GuestID
JOIN Users as u ON u.ID = g.UserID
WHERE rs.DateStayed <= '10-17-2018' AND rs.DateStayed >= '10-01-2018'

-- Add IDENTITY and PRIMARY KEY constraints to SELECT CREATE query
SELECT 
CONCAT('CREATE TABLE ',TABLE_NAME, ' (') as queryPiece 
FROM INFORMATION_SCHEMA.TABLES
 WHERE TABLE_NAME = 'Taverns'
UNION ALL
SELECT CONCAT(cols.COLUMN_NAME, ' ', cols.DATA_TYPE, 
(
	CASE WHEN CHARACTER_MAXIMUM_LENGTH IS NOT NULL 
	Then CONCAT
		('(', CAST(CHARACTER_MAXIMUM_LENGTH as varchar(100)), ')') 
	Else '' 
	END)
, 
	CASE WHEN refConst.CONSTRAINT_NAME IS NOT NULL
	Then 
		(CONCAT(' FOREIGN KEY REFERENCES ', constKeys.TABLE_NAME, '(', constKeys.COLUMN_NAME, ')')) 
	Else '' 
	END
,	
	CASE WHEN cols.IS_NULLABLE = 'NO' AND keys.CONSTRAINT_NAME IS NOT NULL
	Then
		(CONCAT(' IDENTITY (', CAST(IDENT_SEED(cols.TABLE_NAME) as varchar(100)), ',', CAST(IDENT_INCR(cols.TABLE_NAME) as varchar(100)),  ') PRIMARY KEY'))
	Else ''
	END
,
',') as queryPiece FROM 
INFORMATION_SCHEMA.COLUMNS as cols
LEFT JOIN 
INFORMATION_SCHEMA.KEY_COLUMN_USAGE as keys ON 
(keys.TABLE_NAME = cols.TABLE_NAME and keys.COLUMN_NAME = cols.COLUMN_NAME)
LEFT JOIN 
INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS as refConst ON 
(refConst.CONSTRAINT_NAME = keys.CONSTRAINT_NAME)
LEFT JOIN 
(SELECT DISTINCT CONSTRAINT_NAME, TABLE_NAME, COLUMN_NAME 
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE) as constKeys 
ON (constKeys.CONSTRAINT_NAME = refConst.UNIQUE_CONSTRAINT_NAME)
 WHERE cols.TABLE_NAME = 'Taverns'
UNION ALL
SELECT ')';