# SQL

Structured Query Language
*Interface with a relational database*

## Relational Database

Data that is tied to other information.

Example:
```
Sally is a STUDENT who has a MAJOR in Comp Sci.
Each STUDENT has a MAJOR
Each MAJOR has a CLASSes
Each CLASS has GRADES
```

## Transact SQL: TSQL

Basic functionality follows exact sytax, **NOT** executed left to right.

Example:
```
SELECT * 
FROM table 
ORDER BY item 
DESC WHERE something
```

## Normalization

**Primary Keys**
Everything should have an explicit relationship with another table/data

**Example of Non-normalized Table**
```
NAME  | PRICE | QTY
MARK  | 2.99  |  2 
SALLY | 3.99  |  4 
MARK  | 8.99  |  1 
SALLY | 2.99  |  1
```
The table above shows each user/customer in every row they have purchased an item.

**Example of Normalized Tables**
```
CUSTID  |  PRICE  |  QTY
001     |  2.99   |   2
002     |  3.99   |   4 
001     |  8.99   |   1
002     |  2.99   |   1

CUSTID  |  NAME
001     |  MARK
002     |  SALLY
```

*A single table should not contain duplicates of the same information.*

## Creating First SQL Database

**Common SQL Commands**
```
DROP TABLE IF EXISTS [TABLE NAME]; -- Versions 2016+

IF OBJECT_ID (dbo.[TABLE NAME], 'U') IS NOT NULL DROP TABLE dbo.[TABLE NAME]; --U - User-Defined (Versions < 2016)

CREATE TABLE [TABLE NAME] (
	[Column Name][Column Type] IDENTITY ([seed],[increment])
    );

ALTER TABLE [TABLE NAME] (ADD/DROP COLUMN/ALTER COLUMN)
[Column Name](Column Data Type); -- Updating tables
```

**Data Manipulation Commands**
```
INSERT          |   Inserts rows into a table
SELECT          |   Selects attributes from rows in one or more tables/views
    WHERE       |   Restricts the selection of rows based on a conditional expression
    GROUP BY    |   Groups the selected rows based on one or more attributes  
    HAVING      |   Restricts selection of grouped rows based on a condition
    ORDER BY    |   Orders selected rows based on one or more attributes
UPDATE          |   Modifies an attribute's values in one or more table's rows
DELETE          |   Deletes one or more rows from a table
COMMIT          |   Permanently saves data changes
ROLLBACK        |   Restores data to their original values

*Comparison Operators*
=,<,>,<=,>=,<>  |   Used in conditional expressions

*Logical Operators*
AND/OR/NOT      |   Used in conditional expressions

*Special Operators*
BETWEEN         |   Checks that an attribute is within a range
IS NULL         |   Checks if the attribute value is null
LIKE            |   Checks that attribute matches the given string pattern
IN              |   Checks if attribute matches any of the given values
EXISTS          |   Check if the subquery returns any rows
DISTINCT        |   

*Aggregate Functions*
COUNT
MIN
MAX
SUM
AVG
```

**EXISTS**
Use EXISTS to look for info/data that is located in another table.

```
SELECT * FROM Table
WHERE EXISTS
(
    SELECT 1 FROM Inventory
    WHERE Inventory.TavernID = [FantasyTaverns].[TavernID]
)
```

**Entities**
Any objet we want to model and store information about.
* Data is stored in attributes.

*Keys*
Uniquely identifies individual occurrences or entity types.
* Candidate Key - Any column that can be a primary key
* Primary Key - Unique identifier for a table (dates cannot be a primary key)
* Composite Key - When two or more columns are used as a primary key

*Relationships*
Includes one entity from each participating type.
* Type - Meaningful association between entity types
* No limit to relationships (entities can have infinite amount of relationships)

*Cardinality*
* One to one (1:1) - ID or Status
```
Tavern exists at Location
```
* One to many (1:m) - Tavern to Inventory (one tavern has many supplies)
```
Tavern employs multiple Employees
```
* Many to one (m:1) - Inventory to Taverns (multiple supplies associated with one tavern)
```
Bartenders tends to one bar
```
* Many to many (m:m) - Guests to Drinks
```
Patrons consume multiple drinks
```

**Optionality**
A relationship can be optional or mandatory.
* Mandatory - there must be at least one matching record in each entity
* Optional - there may or may not be a matching record in each entity
Declare relationships using FOREIGN KEYS

*Foreign Keys*
Used to declare relationships between entities.

Example:
```
ALTER TABLE [Table] ADD FOREIGN KEY [ColumnName]
REFERENCES [Table]([ColumnName])

ALTER TABLE BasementRats ADD TavernID int FOREIGN KEY REFERENCES Taverns(ID)
```

**Cascade Delete**
ON DELETE CASCADE
* It specifies that the child data is deleted when the parent data is deleted.

Pros:
* Quick
* Foreign Key rows are taken care of

Cons:
* You might orphan records
* Deleting a parent can obliterate a lot of extra data

## Transact SQL - Processing Order

Processing Order:
* FROM
* ON
* OUTER
* WHERE
* GROUP BY
* CUBE | ROLLUP
* HAVING
* SELECT
* DISTINCT
* ORDER BY
* TOP

*COALESCE() Function* - 
Returns the first non-null value in a list
* Example:
```
SELECT COALESCE(Notes, SuperIdentity, 'Oh no') AS UserInfo FROM Users
```

*NULLIF() Function* - 
Returns NULL if condition is met
* Example: If RoleID is 1, NULL will be returned
```
SELECT NULLIF(RoleID, 1) as nullish FROM Users
```

*CAST() Function* - 
Converts the entity into a different type
* Example: RoleIDs will be converted into decimals with 5 places
```
SELECT CAST(RoleID as decimal(18,5)) from Users;
```

*CONCAT() Function* - 
Combines two or more entities together
* Example: Forms a single sentence, "SQL is fun!"
```
SELECT CONCAT('SQL', ' is', ' fun!');
```

*CASE WHEN* - 
Go through conditions and return a value when the first condition is met
* Example: Will determine whether quantity is greater than or less than 30
```
SELECT OrderID, Quantity,
CASE
    WHEN Quantity > 30 THEN "The quantity is greater than 30"
    WHEN Quantity = 30 THEN "The quantity is 30"
    ELSE "The quantity is something else"
END
FROM OrderDetails;
```

*UNION* - 
Combines multiple tables into one resulting table
* Example: Both Roles and GuestStatus will appear in one table
```
SELECT Name, ID FROM Roles
UNION
SELECT Name, ID from GuestStatus
ORDERBY ID
```

*EXCEPT* - 
Will not show results that meet criteria
* Example: Any TavernIDs greater than 5 will not be returned
```
SELECT * FROM Taverns
EXCEPT
SELECT * FROM Taverns
WHERE ID > 5
```

*SELECT * INTO* - 
Creates new table with data from selected table
* Example: Will create a new table called TavernBackups with data coming from Taverns table
```
SELECT * INTO TavernBackups FROM Taverns
```

## JOINS

**4 Types of SQL Joins**
* LEFT JOIN
* RIGHT JOIN
* INNER JOIN
* FULL JOIN

## Functions

Why functions?
* Reusable
* Testable

**Scalar Functions**
```
IF OBJECT_ID (N'dbo.getInventoryStock, N'FN') IS NOT NULL
 DROP FUNCTION getInventoryStock;
GO
CREATE FUNCTION dbo.getInventoryStock(@ProductID int)
RETURNS int
AS
BEGIN
 DECLARE @ret int;
 SELECT @ret = SUM(p.Quantity)
 FROM Production.ProductInventory
 WHERE p.ProductID = @ProductID ;
 IF (@ret IS NULL)
 SET @ret = 0;
 RETURN @ret;
END; 

```

**Table-Values Functions**
```
IF OBJECT_ID (N'Sales.getSalesByStore, N'IF') IS NOT NULL
 DROP FUNCTION Sales.getSalesByStore;
GO
CREATE FUNCTION Sales.getSalesByStore (@storeid int)
RETURNS TABLE
AS
RETURN
(
 SELECT P.ProductID, P.Name, SUM(SD.LineTotal) AS 'Total'
 FROM Production.Product AS P
 JOIN Sales.SalesOrderDetail AS SD ON SD.ProductID = P.ProductID
 JOIN Sales.SalesOrderHeader AS SH ON SH.SalesOrderID = SD.SalesOrderID
 JOIN Sales.Customer AS C ON SH.CustomerID = C.CustomerID
 WHERE C.StoreID = @storeid
 GROUP BY P.ProductID, P.Name
); 

```

**Calling Functions**
```
SELECT ProductModelID, Name, dbo.getInventoryStock(ProductID) AS
CurrentSupply
FROM Production.Product
WHERE ProductModelID BETWEEN 75 and 80;

OR

SELECT * FROM Sales.getSalesByStore (602); 

```

## Procedures - EXPRESS
Similar to a function but CAN modify the database. It can have input and output.

* Usage:
CALL [PROCEDURE]
```
GO
CREATE PROCEDURE Guests.getGuest
@LastName nvarchar(50),
@FirstName nvarchar(50)
AS
/* This can also be an update/create/delete etc */
SET NOCOUNT ON;
SELECT FirstName, LastName, Department
FROM
HumanResources.vEmployeeDepartmentHistory
WHERE FirstName = @FirstName
AND LastName = @LastName
AND EndDate IS NULL;
GO
```

## Triggers - EXPRESS
Triggers are conditional procedures that will run when their conditions are met.

```
CREATE TRIGGER addLevel
ON Guests
AFTER INSERT
AS
INSERT INTO Levels (...) VALUES (...)
GO

```

## Conditionals
You can use conditionals to logically decide what you want returned. This is useful for returning custom
column data.

```
SELECT
 CASE
 WHEN active = 1 THEN ‘Yes’
 WHEN active = 0 THEN ’No’
 ELSE NULL
 END AS activeLabel
FROM Table
```
Useful for reporting - Data manipulation is quickest in SQL vs Application

